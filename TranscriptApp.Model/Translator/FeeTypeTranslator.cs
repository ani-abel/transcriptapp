﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class FeeTypeTranslator: TranslatorBase<FeeType, FEE_TYPE>
    {
        public override FeeType TranslateToModel(FEE_TYPE entity)
        {
            try
            {
                FeeType model = null;
                if (entity != null)
                {
                    model = new FeeType();
                    model.FeeTypeId = entity.FeeTypeId;
                    model.FeeTypeName = entity.FeeTypeName;
                    model.Active = entity.Active;
                }

                return model;
            }
            catch (Exception ex) { throw ex; }
        }

        public override FEE_TYPE TranslateToEntity(FeeType model)
        {
            try
            {
                FEE_TYPE entity = null;
                if (model != null)
                {
                    entity = new FEE_TYPE();
                    entity.FeeTypeId = model.FeeTypeId;
                    entity.FeeTypeName = model.FeeTypeName;
                    entity.Active = model.Active;
                }

                return entity;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
