﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Entity;
using TranscriptApp.Model.Model;

namespace TranscriptApp.Model.Translator
{
    public class PaymentRRRTranslator: TranslatorBase<Payment_RRR, PAYMENT_RRR>
    {
        private PaymentTranslator PaymentTranslator;

        public PaymentRRRTranslator()
        {
            PaymentTranslator = new PaymentTranslator();
        }

        public override PAYMENT_RRR TranslateToEntity(Payment_RRR model)
        {
            try
            {
                PAYMENT_RRR entity = null;
                if (model != null)
                {
                    entity = new PAYMENT_RRR();
                    entity.PaymentRRRId = model.PaymentRRRId;
                    entity.PaymentId = model.Payment.PaymentId;
                    entity.Amount = model.Amount;
                    entity.ReciptNumber = model.RecieptNumber;
                    entity.MerchantCode = model.MerchantCode;
                    entity.CustomerName = model.CustomerName;
                    entity.BankCode = model.BankCode;
                    entity.BranchCode = model.BranchCode;
                    entity.DateCreated = model.DateCreated;
                    if (model.RRR != null)
                    {
                        entity.RRR = model.RRR;
                    }
                    if (model.Status != null)
                    {
                        entity.Status = model.Status;
                    }
                    if (model.Description != null)
                    {
                        entity.Description = model.Description;
                    }
                    if (model.ConfirmationNumber != null)
                    {
                        entity.ConfirmationNumber = model.ConfirmationNumber;
                    }
                    if (model.CustomerAddress != null)
                    {
                        entity.CustomerAddress = model.CustomerAddress;
                    }
                    entity.Used = model.Used;
                    entity.UsedByPersonId = model.UsedByPersonId;
                    entity.TransactionAmount = model.TransactionAmount;
                    entity.OrderId = model.OrderId;
                }

                return entity;
            }
            catch (Exception ex) { throw ex; }
        }

        public override Payment_RRR TranslateToModel(PAYMENT_RRR entity)
        {
            try
            {
                Payment_RRR model = null;
                if (entity != null)
                {
                    model = new Payment_RRR();
                    model.PaymentRRRId = entity.PaymentRRRId;
                    model.Payment = PaymentTranslator.Translate(entity.PAYMENT);
                    model.Amount = entity.Amount;
                    model.RecieptNumber = entity.ReciptNumber;
                    model.MerchantCode = entity.MerchantCode;
                    model.CustomerName = entity.CustomerName;
                    model.BankCode = entity.BankCode;
                    model.BranchCode = entity.BranchCode;
                    model.DateCreated = entity.DateCreated;
                    model.RRR = entity.RRR;
                    model.Status = entity.Status;
                    model.Description = entity.Description;
                    model.TransactionAmount = entity.TransactionAmount;
                    model.ConfirmationNumber = entity.ConfirmationNumber;
                    model.CustomerAddress = entity.CustomerAddress;
                    model.Used = entity.Used;
                    model.UsedByPersonId = entity.UsedByPersonId;
                    model.OrderId = entity.OrderId;
                }

                return model;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
