﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class StateTranslator: TranslatorBase<State, STATE>
    {
        public override STATE TranslateToEntity(State model)
        {
            try
            {
                STATE entity = null;
                if (model != null)
                {
                    entity = new STATE();
                    entity.StateId = model.StateId;
                    entity.StateName = model.StateName;
                    entity.StateCode = model.StateCode;
                }

                return entity;
            }
            catch(Exception ex) { throw ex; }
        }

        public override State TranslateToModel(STATE entity)
        {
            State model = null;
            if (entity != null)
            {
                model = new State();
                model.StateId = entity.StateId;
                model.StateName = entity.StateName;
                model.StateCode = entity.StateCode;
            }

            return model;
        }
    }
}
