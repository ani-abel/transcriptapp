﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class LevelTranslator: TranslatorBase<Level, LEVEL>
    {
        public override LEVEL TranslateToEntity(Level model)
        {
            try
            {
                LEVEL entity = null;
                if (model != null)
                {
                    entity = new LEVEL();
                    entity.LevelId = model.LevelId;
                    entity.LevelName = model.LevelName;
                }

                return entity;
            }
            catch (Exception ex){ throw ex; }
        }

        public override Level TranslateToModel(LEVEL entity)
        {
            try
            {
                Level model = null;
                if (entity != null)
                {
                    model = new Level();
                    model.LevelId = entity.LevelId;
                    model.LevelName = entity.LevelName;
                }

                return model;
            }
            catch (Exception ex) { throw ex; }
        }

    }
}
