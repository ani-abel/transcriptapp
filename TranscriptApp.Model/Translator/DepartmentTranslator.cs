﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class DepartmentTranslator: TranslatorBase<Department, DEPARTMENT>
    {
        public override Department TranslateToModel(DEPARTMENT entity)
        {
            try
            {
                Department model = null;
                if (entity != null)
                {
                    model = new Department();
                    model.DepartmentId = entity.DepartmentId;
                    model.DepartmentName = entity.DepartmentName;
                }

                return model;
            }
            catch(Exception ex) { throw ex; }
        }

        public override DEPARTMENT TranslateToEntity(Department model)
        {
            try
            {
                DEPARTMENT entity = null;
                if (model != null)
                {
                    entity = new DEPARTMENT();
                    model.DepartmentId = entity.DepartmentId;
                    model.DepartmentName = entity.DepartmentName;
                }

                return entity;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
