﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class ProgrammeTranslator: TranslatorBase<Programme, PROGRAMME>
    {
        public override PROGRAMME TranslateToEntity(Programme model)
        {
            try
            {
                PROGRAMME entity = null;
                if (model != null)
                {
                    entity = new PROGRAMME();
                    entity.ProgrammeId = model.ProgrammeId;
                    entity.ProgrammeName = model.ProgrammeName;
                }

                return entity;
            }
            catch(Exception ex) { throw ex; }
        }

        public override Programme TranslateToModel(PROGRAMME entity)
        {
            try
            {
                Programme model = null;
                if (entity != null)
                {
                    model = new Programme();
                    model.ProgrammeId = entity.ProgrammeId;
                    model.ProgrammeName = entity.ProgrammeName;
                }

                return model;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
