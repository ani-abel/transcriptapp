﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class SessionTranslator: TranslatorBase<Session, SESSION>
    {
        public override SESSION TranslateToEntity(Session model)
        {
            try
            {
                SESSION entity = null;
                if (model != null)
                {
                    entity = new SESSION();
                    entity.SessionId = model.SessionId;
                    entity.SessionName = model.SessionName;
                    entity.Active = model.Active;
                }

                return entity;
            }
            catch (Exception ex) { throw ex; }
        }

        public override Session TranslateToModel(SESSION entity)
        {
            try
            {
                Session model = null;
                if (entity != null)
                {
                    model = new Session();
                    model.SessionId = entity.SessionId;
                    model.SessionName = entity.SessionName;
                    model.Active = entity.Active;
                }

                return model;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
