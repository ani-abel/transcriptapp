﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class PaymentModeTranslator: TranslatorBase<PaymentMode, Payment_Mode>
    {
        public override Payment_Mode TranslateToEntity(PaymentMode model)
        {
            try
            {
                Payment_Mode entity = null;
                if (model != null)
                {
                    entity = new Payment_Mode();
                    entity.Payment_Mode_Id = model.Payment_Mode_Id;
                    entity.Payment_Mode_Name = model.Payment_Mode;
                    entity.Payment_Mode_Description = model.Payment_Description;
                }

                return entity;
            }
            catch(Exception ex) { throw ex; }
        }

        public override PaymentMode TranslateToModel(Payment_Mode entity)
        {
            try
            {
                PaymentMode model = null;
                if(entity != null)
                {
                    model = new PaymentMode();
                    model.Payment_Mode_Id = entity.Payment_Mode_Id;
                    model.Payment_Mode = entity.Payment_Mode_Name;
                    model.Payment_Description = entity.Payment_Mode_Description;
                }

                return model;
            }
            catch(Exception ex) { throw ex; }
        }
    }
}
