﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class PersonTranslator: TranslatorBase<Person, PERSON>
    {
        private GenderTranslator GenderTranslator;

        public PersonTranslator()
        {
            GenderTranslator = new GenderTranslator();
        }

        public override PERSON TranslateToEntity(Person model)
        {
            try
            {
                PERSON entity = null;
                if (model != null)
                {
                    entity = new PERSON();
                    entity.PersonId = model.PersonId;
                    entity.Surname = model.Surname;
                    entity.FirstName = model.FirstName;
                    entity.OtherName = model.OtherName;
                    entity.PhoneNumber = model.PhoneNumber;
                    entity.Email = model.Email;
                    entity.MatricNumber = model.MatricNumber;
                    entity.GenderId = model.Gender.GenderId;
                }

                return entity;
            }
            catch (Exception ex) { throw ex; }
        }

        public override Person TranslateToModel(PERSON entity)
        {
            try
            {
                Person model = null;
                if (entity != null)
                {
                    model = new Person();
                    model.PersonId = entity.PersonId;
                    model.Surname = entity.Surname;
                    model.FirstName = entity.FirstName;
                    model.OtherName = entity.OtherName;
                    model.PhoneNumber = entity.PhoneNumber;
                    model.MatricNumber = entity.MatricNumber;
                    model.Gender = GenderTranslator.Translate(entity.GENDER);
                    model.Email = entity.Email;
                }

                return model;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}