﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Entity;
using TranscriptApp.Model.Model;

namespace TranscriptApp.Model.Translator
{
    public class RequestTranslator:TranslatorBase<Request, REQUEST>
    {
        private CountryTranslator CountryTranslator;
        private PersonTranslator PersonTranslator;
        private FeeTypeTranslator FeeTypeTranslator;
        private DepartmentTranslator DepartmentTranslator;
        private ProgrammeTranslator ProgrammeTranslator;
        private DeliveryServiceTranslator DeliveryServiceTranslator;
        private LevelTranslator LevelTranslator;

        public RequestTranslator()
        {
            CountryTranslator = new CountryTranslator();
            PersonTranslator = new PersonTranslator();
            FeeTypeTranslator = new FeeTypeTranslator();
            DepartmentTranslator = new DepartmentTranslator();
            ProgrammeTranslator = new ProgrammeTranslator();
            DeliveryServiceTranslator = new DeliveryServiceTranslator();
            LevelTranslator = new LevelTranslator();
        }

        public override REQUEST TranslateToEntity(Request model)
        {
            try
            {
                REQUEST entity = null;
                if (model != null)
                {
                    entity = new REQUEST();
                    entity.RequestId = model.RequestId;
                    entity.CountryId = model.Country.CountryId;
                    entity.LevelId = model.Level.LevelId;
                    entity.DepartmentId = model.Department.DepartmentId;
                    entity.DestinationAddress = model.DestinationAddress;
                    if (model.State != null && model.State.StateId > 0)
                    {
                        entity.StateId = model.State.StateCode;
                    }
                    entity.ProgrammeId = model.Programme.ProgrammeId;
                    entity.FeeTypeId = model.FeeType.FeeTypeId;
                    entity.DateCreated = model.DateCreated;
                    if(model.DeliveryService != null && model.DeliveryService.DeliveryServiceId > 0)
                    {
                        entity.DeliveryServiceId = model.DeliveryService.DeliveryServiceId;
                    }
                    
                    entity.PersonId = model.Person.PersonId;
                }

                return entity;
            }
            catch (Exception ex) { throw ex; }
        }

        public override Request TranslateToModel(REQUEST entity)
        {
            try
            {
                Request model = null;
                if (entity != null)
                {
                    model = new Request();
                    model.RequestId = entity.RequestId;
                    model.Country = CountryTranslator.Translate(entity.COUNTRY);
                    model.Level = LevelTranslator.Translate(entity.LEVEL);
                    model.Department = DepartmentTranslator.Translate(entity.DEPARTMENT);
                    model.DestinationAddress = entity.DestinationAddress;
                    model.Programme = ProgrammeTranslator.Translate(entity.PROGRAMME);
                    model.FeeType = FeeTypeTranslator.Translate(entity.FEE_TYPE);
                    model.DateCreated = entity.DateCreated;
                    model.DeliveryService = DeliveryServiceTranslator.Translate(entity.DELIVERY_SERVICE);
                    model.Person = PersonTranslator.Translate(entity.PERSON);
                }

                return model;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
