﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class PaymentTranslator: TranslatorBase<Payment, PAYMENT>
    {
        private FeeTypeTranslator FeeTypeTranslator;
        private PersonTranslator PersonTranslator;
        private RequestTranslator RequestTranslator;
        private PaymentModeTranslator PaymentModeTranslator;
        private PersonTypeTranslator PersonTypeTranslator;
        private PaymentTypeTranslator PaymentTypeTranslator;
        private SessionTranslator SessionTranslator;

        public PaymentTranslator()
        {
            FeeTypeTranslator = new FeeTypeTranslator();
            PersonTranslator = new PersonTranslator();
            RequestTranslator = new RequestTranslator();
            PaymentModeTranslator = new PaymentModeTranslator();
            PersonTypeTranslator = new PersonTypeTranslator();
            PaymentTypeTranslator = new PaymentTypeTranslator();
            SessionTranslator = new SessionTranslator();
        }

        public override PAYMENT TranslateToEntity(Payment model)
        {
            try
            {
                PAYMENT entity = null;
                if (model != null)
                {
                    entity = new PAYMENT();
                    entity.PaymentId = model.PaymentId;
                    entity.AmountPaid = model.AmountPaid;
                    entity.FeeTypeId = model.FeeType.FeeTypeId;
                    entity.PersonId = model.Person.PersonId;
                    entity.InvoiceNumber = model.InvoiceNumber;
                    entity.DateCreated = model.DateCreated;
                    if (model.RRR != null)
                    {
                        entity.RRR = model.RRR;
                    }
                    if (model.Request != null && model.Request.RequestId > 0)
                    {
                        entity.RequestId = model.Request.RequestId;
                    }
                    if (model.PaymentType != null && model.PaymentType.PaymentTypeId > 0)
                    {
                        entity.PaymentTypeId = model.PaymentType.PaymentTypeId;
                    }
                    if (model.PersonType != null && model.PersonType.PersonTypeId > 0)
                    {
                        entity.PersonTypeId = model.PersonType.PersonTypeId;
                    }
                    if (model.Session != null && model.Session.SessionId > 0)
                    {
                        entity.SessionId = model.Session.SessionId;
                    }
                }

                return entity;
            }
            catch (Exception ex) { throw ex; }
        }

        public override Payment TranslateToModel(PAYMENT entity)
        {
            try
            {
                Payment model = null;
                if (entity != null)
                {
                    model = new Payment();
                    model.PaymentId = entity.PaymentId;
                    model.AmountPaid = entity.AmountPaid;
                    model.FeeType = FeeTypeTranslator.Translate(entity.FEE_TYPE);
                    model.Person = PersonTranslator.Translate(entity.PERSON);
                    model.InvoiceNumber = entity.InvoiceNumber;
                    model.DateCreated = entity.DateCreated;
                    model.Request = RequestTranslator.Translate(entity.REQUEST);
                    model.PaymentType = PaymentTypeTranslator.Translate(entity.PaymentType);
                    model.PersonType = PersonTypeTranslator.Translate(entity.Person_Type);
                    model.Session = SessionTranslator.Translate(entity.SESSION);
                }

                return model;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
