﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Entity;
using TranscriptApp.Model.Model;

namespace TranscriptApp.Model.Translator
{
    public class CountryTranslator: TranslatorBase<Country, COUNTRY>
    {

        public override Country TranslateToModel(COUNTRY entity)
        {
            try
            {
                Country model = null;
                if (entity != null)
                {
                    model = new Country();
                    model.CountryId = entity.CountryId;
                    model.CountryName = entity.CountryName;
                }

                return model;
            }
            catch(Exception ex) { throw ex; }
        }


        public override COUNTRY TranslateToEntity(Country model)
        {
            try
            {
                COUNTRY entity = null;
                if (model != null)
                {
                    entity = new COUNTRY();
                    entity.CountryId = model.CountryId;
                    entity.CountryName = model.CountryName;
                }

                return entity;
            }
            catch(Exception ex) { throw ex; }
        }
        
    }
}
