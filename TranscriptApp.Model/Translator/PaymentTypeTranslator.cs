﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Entity;
using TranscriptApp.Model.Model;

namespace TranscriptApp.Model.Translator
{
    public class PaymentTypeTranslator: TranslatorBase<Payment_Type, PaymentType>
    {
        public override PaymentType TranslateToEntity(Payment_Type model)
        {
            try
            {
                PaymentType entity = null;
                if(model != null)
                {
                    entity = new PaymentType();
                    entity.Payment_Type_Id = model.PaymentTypeId;
                    entity.Payment_Name = model.PaymentName;
                    entity.Payment_Description = model.PaymentDescription;
                }

                return entity;
            }
            catch(Exception ex) { throw ex; }
        }

        public override Payment_Type TranslateToModel(PaymentType entity)
        {
            try
            {
                Payment_Type model = null;
                if(entity != null)
                {
                    model = new Payment_Type();
                    model.PaymentTypeId = entity.Payment_Type_Id;
                    model.PaymentName = entity.Payment_Name;
                    model.PaymentDescription = entity.Payment_Description;
                }

                return model;
            }
            catch(Exception ex) { throw ex; }
        }
    }
}