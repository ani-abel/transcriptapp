﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class PersonTypeTranslator: TranslatorBase<PersonType, Person_Type>
    {
        public override Person_Type TranslateToEntity(PersonType model)
        {
            try
            {
                Person_Type entity = null;
                if (model != null)
                {
                    entity = new Person_Type();
                    entity.Person_Type_Id = model.PersonTypeId;
                    entity.Person_Type_Name = model.PersonTypeName;
                    entity.Person_Type_Description = model.PersonTypeDescription;
                }

                return entity;
            }
            catch(Exception ex) { throw ex; }
        }


        public override PersonType TranslateToModel(Person_Type entity)
        {
            try
            {
                PersonType model = null;
                if (entity != null)
                {
                    model = new PersonType();
                    model.PersonTypeId = entity.Person_Type_Id;
                    model.PersonTypeName = entity.Person_Type_Name;
                    model.PersonTypeDescription = entity.Person_Type_Description;
                }

                return model;
            }
            catch(Exception ex) { throw ex; }
        }
    }
}
