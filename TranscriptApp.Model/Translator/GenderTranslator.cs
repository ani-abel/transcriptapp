﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class GenderTranslator: TranslatorBase<Gender, GENDER>
    {
        public override GENDER TranslateToEntity(Gender model)
        {
            try
            {
                GENDER entity = null;
                if (model != null)
                {
                    entity = new GENDER();
                    entity.GenderId = model.GenderId;
                    entity.GenderName = model.GenderName;
                }
                return entity;
            }
            catch (Exception ex) { throw ex; }
        }

        public override Gender TranslateToModel(GENDER entity)
        {
            try
            {
                Gender model = null;
                if (entity != null)
                {
                    model = new Gender();
                    model.GenderId = entity.GenderId;
                    model.GenderName = entity.GenderName;
                }
                return model;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
