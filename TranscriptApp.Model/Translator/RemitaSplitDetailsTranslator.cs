﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Entity;
using TranscriptApp.Model.Model;

namespace TranscriptApp.Model.Translator
{
    public class RemitaSplitDetailsTranslator: TranslatorBase<RemitaSplitDetails, REMITA_SPLIT_DETAILS>
    {
        public override REMITA_SPLIT_DETAILS TranslateToEntity(RemitaSplitDetails model)
        {
            try
            {
                REMITA_SPLIT_DETAILS entity = null;
                if (model != null)
                {
                    entity = new REMITA_SPLIT_DETAILS();
                    entity.Id = model.Id;
                    entity.Active = model.Active;
                    entity.BankCode = model.BankCode;
                    entity.BeneficiaryAccount = model.BeneficiaryAccount;
                    entity.BeneficiaryAmount = model.BeneficiaryAmount;
                    entity.BeneficiaryName = model.BeneficiaryName;
                }

                return entity;
            }
            catch(Exception ex) { throw ex; }
        }

        public override RemitaSplitDetails TranslateToModel(REMITA_SPLIT_DETAILS entity)
        {
            try
            {
                RemitaSplitDetails model = null;
                if (entity != null)
                {
                    model = new RemitaSplitDetails();
                    model.Id = entity.Id;
                    model.Active = entity.Active;
                    model.BankCode = entity.BankCode;
                    model.BeneficiaryAccount = entity.BeneficiaryAccount;
                    model.BeneficiaryAmount = model.BeneficiaryAmount;
                    model.BeneficiaryName = model.BeneficiaryName;
                }

                return model;
            }
            catch(Exception ex) { throw ex; }
        }
    }
}
