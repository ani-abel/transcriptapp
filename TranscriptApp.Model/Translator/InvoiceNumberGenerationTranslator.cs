﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class InvoiceNumberGenerationTranslator: TranslatorBase<InvoiceNumberGeneration, INVOICE_NUMBER_GENERATION>
    {
        public override INVOICE_NUMBER_GENERATION TranslateToEntity(InvoiceNumberGeneration model)
        {
            INVOICE_NUMBER_GENERATION entity = null;
            if (model != null)
            {
                entity = new INVOICE_NUMBER_GENERATION();
                entity.GenerationId = model.GenerationId;
                entity.Prefix = model.Prefix;
                entity.StartFrom = model.StartFrom;
                entity.Active = model.Active;
            }

            return entity;
        }

        public override InvoiceNumberGeneration TranslateToModel(INVOICE_NUMBER_GENERATION entity)
        {
            InvoiceNumberGeneration model = null;
            if (entity != null)
            {
                model = new InvoiceNumberGeneration();
                model.GenerationId = entity.GenerationId;
                model.Prefix = entity.Prefix;
                model.StartFrom = entity.StartFrom;
                model.Active = entity.Active;
            }

            return model;
        }
    }
}
