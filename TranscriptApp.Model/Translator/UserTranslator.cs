﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class UserTranslator: TranslatorBase<User, USER>
    {
        public override USER TranslateToEntity(User model)
        {
            try
            {
                USER entity = null;
                if (model != null)
                {
                    entity = new USER();
                    entity.UserId = model.UserId;
                    entity.Username = model.Username;
                    entity.Password = model.Password;
                }
                return entity;
            }
            catch(Exception ex) { throw ex; }
        }

        public override User TranslateToModel(USER entity)
        {
            try
            {
                User model = null;
                if (entity != null)
                {
                    model = new User();
                    model.UserId = entity.UserId;
                    model.Username = entity.Username;
                    model.Password = entity.Password;
                }
                return model;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
