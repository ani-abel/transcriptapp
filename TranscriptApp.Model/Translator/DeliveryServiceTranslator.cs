﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class DeliveryServiceTranslator: TranslatorBase<DeliveryService, DELIVERY_SERVICE>
    {

        public override DELIVERY_SERVICE TranslateToEntity(DeliveryService model)
        {
            try
            {
                DELIVERY_SERVICE entity = null;
                if (model != null)
                {
                    entity = new DELIVERY_SERVICE();
                    entity.DeliveryServiceId= model.DeliveryServiceId;
                    entity.DeliveryServiceName = model.DeliveryServiceName;
                }

                return entity;
            }
            catch (Exception ex) { throw ex; }
        }

        public override DeliveryService TranslateToModel(DELIVERY_SERVICE entity)
        {
            try
            {
                DeliveryService model = null;
                if (entity != null)
                {
                    model = new DeliveryService();
                    model.DeliveryServiceId = entity.DeliveryServiceId;
                    model.DeliveryServiceName = entity.DeliveryServiceName;
                }

                return model;
            }
            catch(Exception ex) { throw ex; }
        }
    }
}
