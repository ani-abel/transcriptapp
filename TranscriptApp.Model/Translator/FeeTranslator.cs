﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Entity;
using TranscriptApp.Model.Model;

namespace TranscriptApp.Model.Translator
{
    public class FeeTranslator: TranslatorBase<Fee, FEE>
    {
        public override FEE TranslateToEntity(Fee model)
        {
            try
            {
                FEE entity = null;
                if (model != null)
                {
                    entity = new FEE();
                    entity.FeeId = model.FeeId;
                    entity.Amount = model.Amount;
                    entity.Active = model.Active;
                }

                return entity;
            }
            catch(Exception ex) { throw ex; }
        }

        public override Fee TranslateToModel(FEE entity)
        {
            try
            {
                Fee model = null;
                if (entity != null)
                {
                    model = new Fee();
                    model.FeeId = entity.FeeId;
                    model.Amount = entity.Amount;
                    model.Active = entity.Active;
                }

                return model;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
