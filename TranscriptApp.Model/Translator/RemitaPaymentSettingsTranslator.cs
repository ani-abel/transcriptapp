﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class RemitaPaymentSettingsTranslator: TranslatorBase<RemitaPaymentSettings, REMITA_PAYMENT_SETTINGS>
    {
        public override REMITA_PAYMENT_SETTINGS TranslateToEntity(RemitaPaymentSettings model)
        {
            try
            {
                REMITA_PAYMENT_SETTINGS entity = null;
                if (model != null)
                {
                    entity = new REMITA_PAYMENT_SETTINGS();
                    entity.Id = model.Id;
                    entity.Api_Key = model.Api_Key;
                    entity.Description = model.Description;
                    entity.MerchantId = model.MerchantId;
                    entity.Response_Url = model.Response_Url;
                    entity.ServiceTypeId = model.ServiceTypeId;
                }

                return entity;
            }
            catch(Exception ex) { throw ex; }
        }

        public override RemitaPaymentSettings TranslateToModel(REMITA_PAYMENT_SETTINGS entity)
        {
            try
            {
                RemitaPaymentSettings model = null;
                if (entity != null)
                {
                    model = new RemitaPaymentSettings();
                    model.Id = entity.Id;
                    model.Api_Key = entity.Api_Key;
                    model.Description = entity.Description;
                    model.MerchantId = entity.MerchantId;
                    model.Response_Url = entity.Response_Url;
                    model.ServiceTypeId = entity.ServiceTypeId;
                }

                return model;
            }
            catch(Exception ex) { throw ex; }
        }
    }
}
