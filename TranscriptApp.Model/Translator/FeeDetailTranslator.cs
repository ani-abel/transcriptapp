﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Model.Translator
{
    public class FeeDetailTranslator:TranslatorBase<FeeDetail, FEE_DETAIL>
    {
        private FeeTranslator feeTranslator;
        private FeeTypeTranslator feeTypeTranslator;
        private SessionTranslator sessionTranslator;

        public FeeDetailTranslator()
        {
            feeTypeTranslator = new FeeTypeTranslator();
            feeTranslator = new FeeTranslator();
            sessionTranslator = new SessionTranslator();
        }

        public override FEE_DETAIL TranslateToEntity(FeeDetail model)
        {
            try
            {
                FEE_DETAIL entity = null;
                if (model != null)
                {
                    entity = new FEE_DETAIL();
                    entity.FeeDetailId = model.FeeDetailId;
                    entity.FeeId = model.Fee.FeeId;
                    entity.FeeTypeId = model.FeeType.FeeTypeId;
                    entity.SessionId = model.Session.SessionId;
                    entity.Active = model.Active;
                }

                return entity;
            }
            catch (Exception ex) { throw ex; }
        }

        public override FeeDetail TranslateToModel(FEE_DETAIL entity)
        {
            try
            {
                FeeDetail model = null;
                if (entity != null)
                {
                    model = new FeeDetail();
                    model.FeeDetailId = entity.FeeDetailId;
                    model.Fee = feeTranslator.Translate(entity.FEE);
                    model.FeeType = feeTypeTranslator.Translate(entity.FEE_TYPE);
                    model.Session = sessionTranslator.Translate(entity.SESSION);
                    model.Active = entity.Active;
                }

                return model;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
