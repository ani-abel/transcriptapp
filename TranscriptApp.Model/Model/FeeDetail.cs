﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class FeeDetail
    {
        public int FeeDetailId { get; set; }

        public Fee Fee { get; set; }//Stand-in for FeeId

        public FeeType FeeType { get; set; }

        public Session Session { get; set; }

        public bool Active { get; set; }
    }
}
