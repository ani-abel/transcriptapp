﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class Session
    {
        public int SessionId { get; set; }

        [DisplayName("Session")]
        public string SessionName { get; set; }

        public bool Active { get; set; }
    }
}
