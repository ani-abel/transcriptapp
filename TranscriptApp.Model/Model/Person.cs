﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class Person
    {
        public int PersonId { get; set; }

        public string Surname { get; set; }

        public string FirstName { get; set; }

        public string OtherName { get; set; }

        public string PhoneNumber { get; set; }

        public Gender Gender { get; set; }

        public string Email { get; set; }

        public string MatricNumber { get; set; }

    }
}
