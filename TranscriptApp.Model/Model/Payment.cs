﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class Payment
    {
        public int PaymentId { get; set; }

        public FeeType FeeType { get; set; }

        public Person Person { get; set; }

        [DisplayName("Amount")]
        public Nullable<decimal> AmountPaid { get; set; }

        [DisplayName("Invoice Number")]
        public string InvoiceNumber { get; set; }

        public DateTime DateCreated { get; set; }

        public string RRR { get; set; }

        public Request Request { get; set; }

        public Session Session { get; set; }

        public PersonType PersonType { get; set; }

        public Payment_Type PaymentType { get; set; }
    }
}
