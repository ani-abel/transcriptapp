﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class Request
    {
        public int RequestId { get; set; }

        public Level Level { get; set; }//stand-in for LevelId, Nullable

        public Department Department { get; set; }

        public Programme Programme { get; set; }

        public string DestinationAddress { get; set; }

        public Country Country { get; set; }

        public State State { get; set; }

        public Person Person { get; set; }

        public DeliveryService DeliveryService { get; set; }

        public FeeType FeeType { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
