﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class RemitaPaymentSettings
    {
        public int Id { get; set; }

        public string MerchantId { get; set; }

        public string Api_Key { get; set; }

        public string ServiceTypeId { get; set; }

        public string Response_Url { get; set; }

        public string Description { get; set; }
    }
}
