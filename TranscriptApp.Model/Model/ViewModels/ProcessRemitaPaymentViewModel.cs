﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class ProcessRemitaPaymentViewModel
    {
        public int PaymentId { get; set; }

        public string RemitaPayerName { get; set; }

        public string RemitaPayerEmail { get; set; }

        public string RemitaPayerPhone { get; set; }

        public string RemitaOrderId { get; set; }

        public string RemitaMerchantId { get; set; }

        public string RemitaServiceTypeId { get; set; }

        public string RemitaResponseURL { get; set; }

        public decimal RemitaPaymentTotalAmount { get; set; }

        public string RemitaAmount { get; set; }

        public string RemitaHash { get; set; }

        public string RemitaRRR { get; set; }

        public string PaymentType { get; set; }
    }
}
