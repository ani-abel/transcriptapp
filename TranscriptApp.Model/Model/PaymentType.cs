﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class Payment_Type
    {
        public int PaymentTypeId { get; set; }

        public string PaymentName { get; set; }

        public string PaymentDescription { get; set; }
    }
}
