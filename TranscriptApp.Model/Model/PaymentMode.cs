﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class PaymentMode
    {
        public int Payment_Mode_Id { get; set; }

        public string Payment_Mode { get; set; }

        public string Payment_Description { get; set; }
    }
}
