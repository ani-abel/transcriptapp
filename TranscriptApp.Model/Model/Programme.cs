﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class Programme
    {
        public int ProgrammeId { get; set; }

        [DisplayName("Programme")]
        public string ProgrammeName { get; set; }
    }
}
