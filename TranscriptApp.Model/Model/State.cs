﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class State
    {
        public int StateId { get; set; }

        [DisplayName("State")]
        public string StateName { get; set; }

        public string StateCode { get; set; }
    }
}
