﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class JsonPayload
    {
        public string Message { get; set; }

        public bool WasSuccessFul { get; set; }
    }
}
