﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class InvoiceNumberGeneration
    {
        public int GenerationId { get; set; }

        public string Prefix { set; get; }

        public long StartFrom { get; set; }

        public bool Active { get; set; }
    }
}
