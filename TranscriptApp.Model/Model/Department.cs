﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class Department
    {
        public int DepartmentId { get; set; }

        [DisplayName("Department")]
        public string DepartmentName { get; set; }
    }
}
