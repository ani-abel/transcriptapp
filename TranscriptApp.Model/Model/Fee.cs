﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class Fee
    {
        public int FeeId { get; set; }

        public decimal Amount { get; set; }

        public bool Active { get; set; }
    }
}
