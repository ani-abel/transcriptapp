﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public abstract class Setup
    {
        [Required]
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }
    }
}
