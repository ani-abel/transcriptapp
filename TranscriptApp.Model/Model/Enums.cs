﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public enum PersonTypeList
    {
        Student = 1,
        Graduate = 2
    }

    public enum PaymentTypeList
    {
        CardPayment = 1,
        CashPayment = 2
    }
}
