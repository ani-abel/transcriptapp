﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class DeliveryService
    {
        public int DeliveryServiceId { get; set; }

        [DisplayName("Delivery Service")]
        public string DeliveryServiceName { get; set; }
    }
}
