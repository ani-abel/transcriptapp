﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class Country
    {
        public int CountryId { get; set; }

        [DisplayName("Country")]
        public string CountryName { get; set; }
    }
}
