﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class PersonType
    {
        public int PersonTypeId { get; set; }
        public string PersonTypeName { get; set; }
        public string PersonTypeDescription { get; set; }
    }
}
