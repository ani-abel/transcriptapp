﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class Gender
    {
        public int GenderId { get; set; }

        [DisplayName("Gender")]
        public string GenderName { get; set; }
    }
}
