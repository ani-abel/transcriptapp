﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class Level
    {
        public int LevelId { get; set; }
        
        [DisplayName("Level")]
        public string LevelName { set; get; }
    }
}
