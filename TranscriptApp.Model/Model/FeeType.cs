﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class FeeType
    {
        public int FeeTypeId { get; set; }

        [DisplayName("Fee Type")]
        public string FeeTypeName { get; set; }

        public bool Active { get; set; }
    }
}
