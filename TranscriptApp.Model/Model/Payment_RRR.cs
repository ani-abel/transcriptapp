﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscriptApp.Model.Model
{
    public class Payment_RRR
    {
        public int PaymentRRRId { get; set; }

        public Payment Payment { get; set; }//stand-in for PaymentId

        public Nullable<decimal> Amount { get; set; }

        public string RecieptNumber { get; set; }

        public string MerchantCode { get; set; }

        public string CustomerName { get; set; }

        public string BankCode { get; set; }

        public string BranchCode { get; set; }

        public DateTime DateCreated { get; set; }

        public string RRR { get; set; }

        public string Status { get; set; }

        public string Description { get; set; }

        public decimal TransactionAmount { get; set; }

        public string ConfirmationNumber { get; set; }

        public string CustomerAddress { get; set; }

        public Nullable<bool> Used { get; set; }

        public Nullable<long> UsedByPersonId { get; set; }

        public string OrderId { get; set; }
    }
}
