﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TranscriptApp.Business;
using TranscriptApp.Model.Model;

namespace TranscriptApp.Web.Models
{
    public class TranscriptRequestViewModel
    {
        [Required(ErrorMessage = "Matric number is required")]
        [Display(Name = "Matric Number")]
        public string MatricNumber { get; set; }

        [Required(ErrorMessage = "Surname is required")]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "Other Name")]
        public string OtherName { get; set; }

        [Required(ErrorMessage = "First name is required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Must be valid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Mobile number is required")]
        [Display(Name = "Mobile Number")]
        [DataType(DataType.PhoneNumber)]
        public string MobileNo { get; set; }

        public int Programme { get; set; }

        public int Gender { get; set; }

        public int Level { get; set; }

        [Required(ErrorMessage = "Destination Country is required")]
        [Display(Name = "Destination Country")]
        public int DestinationCountry { get; set; }

        [Required(ErrorMessage = "Destination Address is required")]

        [Display(Name = "Destination Address")]
        public string DestinationLocation { get; set; }//tells us the faculty to which the request is intended

        public int Department { get; set; }

        public int RequestType { get; set; }

        public bool IsCourierEnabled { get; set; }

        public Nullable<int> Courier { get; set; }

        public string State { get; set; }
        
        public List<EarlierTranscriptsViewModel> EarlierTranscripts { get; set; } = new List<EarlierTranscriptsViewModel>();
        
        public List<Value> GetGenders()
        {
            try
            {
                var GenderLogic = new GenderLogic();
                List<Gender> Genders = GenderLogic.GetAll();
                List<Value> Values = new List<Value>();

                if (Genders != null && Genders.Count > 0)
                {
                    for (int i = 0; i < Genders.Count; i++)
                    {
                        Values.Add(new Value() { Id = Genders[i].GenderId, ValueText = Genders[i].GenderName });
                    }
                }
                return Values;
            
            }
            catch(Exception ex) { throw ex; }
        }

        public List<Value> GetProgrammes()
        {
            try
            {
                var ProgrammeLogic = new ProgrammeLogic();
                List<Programme> Programmmes = ProgrammeLogic.GetAll();
                List<Value> Values = new List<Value>();

                if (Programmmes != null && Programmmes.Count() > 0)
                {
                    for (int i = 0; i < Programmmes.Count(); i++)
                    {
                        Values.Add(new Value() { Id = Programmmes[i].ProgrammeId, ValueText = Programmmes[i].ProgrammeName });
                    }
                }
                return Values;

            }
            catch (Exception ex) { throw ex; }
        }

        public List<Value> GetLevels()
        {
            try
            {
                var LevelLogic = new LevelLogic();
                List<Level> Levels = LevelLogic.GetAll();
                List<Value> Values = new List<Value>();

                if (Levels != null && Levels.Count() > 0)
                {
                    for (int i = 0; i < Levels.Count(); i++)
                    {
                        Values.Add(new Value() { Id = Levels[i].LevelId, ValueText = Levels[i].LevelName });
                    }
                }
                return Values;

            }
            catch (Exception ex) { throw ex; }
        }

        public List<Value> GetDeliveryServices()
        {
            try
            {
                var DeliveryServiceLogic = new DeliveryServiceLogic();
                List<DeliveryService> DeliveryServices = DeliveryServiceLogic.GetAll();
                List<Value> Values = new List<Value>();

                if (DeliveryServices != null && DeliveryServices.Count() > 0)
                {
                    for (int i = 0; i < DeliveryServices.Count(); i++)
                    {
                        Values.Add(new Value() { Id = DeliveryServices[i].DeliveryServiceId, ValueText = DeliveryServices[i].DeliveryServiceName });
                    }
                }
                return Values;

            }
            catch (Exception ex) { throw ex; }
        }

        public List<Value> GetFeeTypes()
        {
            try
            {
                var FeeTypeLogic = new FeeTypeLogic();
                List<FeeType> FeeTypes = FeeTypeLogic.GetModelsBy(ft => ft.Active);
                List<Value> Values = new List<Value>();

                if (FeeTypes != null && FeeTypes.Count() > 0)
                {
                    for (int i = 0; i < FeeTypes.Count(); i++)
                    {
                        Values.Add(new Value() { Id = FeeTypes[i].FeeTypeId, ValueText = FeeTypes[i].FeeTypeName });
                    }
                }
                return Values;

            }
            catch (Exception ex) { throw ex; }
        }

        public List<Value> GetStates()
        {
            try
            {
                var StatesLogic = new StateLogic();
                List<State> States = StatesLogic.GetAll();
                List<Value> Values = new List<Value>();

                if (States != null && States.Count() > 0)
                {
                    for (int i = 0; i < States.Count(); i++)
                    {
                        Values.Add(new Value() { ValueCode = States[i].StateCode, ValueText = States[i].StateName });
                    }
                }
                return Values;

            }
            catch (Exception ex) { throw ex; }
        }

        public List<Value> GetCountries()
        {
            try
            {
                var CountriesLogic = new CountryLogic();
                List<Country> Countries = CountriesLogic.GetAll().OrderBy(c => c.CountryName).ToList();
                List<Value> Values = new List<Value>();

                if (Countries != null &&Countries.Count() > 0)
                {
                    for (int i = 0; i < Countries.Count(); i++)
                    {
                        Values.Add(new Value() { Id = Countries[i].CountryId, ValueText = Countries[i].CountryName });
                    }
                }
                return Values;

            }
            catch (Exception ex) { throw ex; }
        }

        public List<Value> GetDepartments()
        {
            try
            {
                var DepartmentsLogic = new DepartmentLogic();
                List<Department> Departments = DepartmentsLogic.GetAll();
                List<Value> Values = new List<Value>();

                if (Departments != null && Departments.Count() > 0)
                {
                    for (int i = 0; i < Departments.Count(); i++)
                    {
                        Values.Add(new Value() { Id = Departments[i].DepartmentId, ValueText = Departments[i].DepartmentName });
                    }
                }
                return Values;

            }
            catch (Exception ex) { throw ex; }
        }

    }

    public class EarlierTranscriptsViewModel
    {
        public long PaymentId { get; set; }

        public string RequestType { get; set; }

        public string MobileNumber { get; set; }

        public string Programme { get; set; }

        public string Level { get; set; }

        public string Country { get; set; }

        public string Department { get; set; }

        public string RequestDetination { get; set; }

        public string Email { get; set; }

        public string DateCreated { get; set; }
    }
    
}