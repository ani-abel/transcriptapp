﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TranscriptApp.Web.Models
{
    public class ViewInvoiceViewModel
    {
        public string Person { get; set; }

        public Nullable<decimal> Amount { get; set; }

        public DateTime DateCreated { get; set; }

        public string RRR { get; set; }
    }
}