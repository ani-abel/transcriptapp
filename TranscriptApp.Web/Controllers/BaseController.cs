﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TranscriptApp.Model.Model;
using TranscriptApp.Business;
using System.Web.Mvc;

namespace TranscriptApp.Web.Controllers
{
    public class BaseController: Controller
    {
        protected void SetMessage(string message, Message.Category messageType)
        {
            Message msg = new Message(message, (int)messageType);
            TempData["Message"] = msg;

        }
    }
    
}