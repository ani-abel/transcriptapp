﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Transactions;
using TranscriptApp.Business;
using TranscriptApp.Model.Model;
using TranscriptApp.Web.Models;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;

namespace TranscriptApp.Web.Controllers
{
    public class HomeController : BaseController
    {
        private TranscriptRequestViewModel viewModel;

        public HomeController()
        {
            viewModel = new TranscriptRequestViewModel();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Transcript()
        {
            try
            {
                if (TempData["MatricNumber"] != null)
                {
                    viewModel.MatricNumber = (string)TempData["MatricNumber"];
                }

                LoadviewBags();

                //preload the data from the Db
                var personLogic = new PersonLogic();
                var person = personLogic.GetModelsBy(p => p.MatricNumber == viewModel.MatricNumber).LastOrDefault();
                if (person != null)
                {
                    viewModel.Surname = person.Surname;
                    viewModel.FirstName = person.FirstName;
                    viewModel.OtherName = person.OtherName;
                    viewModel.MobileNo = person.PhoneNumber;
                    viewModel.Gender = person.Gender.GenderId;
                    viewModel.Email = person.Email;

                    //use the person Id to query the tbl.Request for more Transcript request made by this person
                    //EarlierTranscripts
                    var requestLogic = new RequestLogic();
                    var requests = requestLogic.GetModelsBy(req => req.PersonId == person.PersonId);
                    //get the paymentLogic
                    var paymentLogic = new PaymentLogic();

                    if (requests != null && requests.Count() > 0)
                    {
                        for (int i = 0; i < requests.Count(); i++)
                        {
                            var currentReq = requests[i];

                            EarlierTranscriptsViewModel model = new EarlierTranscriptsViewModel();
                            model.PaymentId = paymentLogic.GetModelsBy(p => p.PersonId == person.PersonId && p.RequestId == currentReq.RequestId).LastOrDefault().PaymentId;
                            model.Country = currentReq.Country.CountryName;
                            model.DateCreated = TimeSpanLogic.RelativeDate(currentReq.DateCreated);
                            model.Department = currentReq.Department.DepartmentName;
                            model.Programme = currentReq.Programme.ProgrammeName;
                            model.MobileNumber = currentReq.Person.PhoneNumber;
                            model.Email = currentReq.Person.Email;
                            model.Level = currentReq.Level.LevelName;
                            model.RequestType = currentReq.FeeType.FeeTypeName;
                            model.RequestDetination = currentReq.DestinationAddress;

                            viewModel.EarlierTranscripts.Add(model);
                        }
                    }
                }

                return View(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Transcript(TranscriptRequestViewModel viewModel)
        {
            try
            {
                //var errors = ModelState.Values.Any(x => x.Errors.Count() >= 1);
                //Used to extract which field on a model has an error
                //var errorList = ModelState.Values.SelectMany(x => x.Errors);

                if (ModelState.IsValid)
                {
                    var payment = CreateTranscriptRequest(viewModel);

                    TempData["MatricNumber"] = viewModel.MatricNumber;

                    if (payment != null)
                    {
                        //After creating Payments redirectTo: "ProcessRemitaPayment"
                        return RedirectToAction("ProcessRemitaPayment", new { Id = payment.PaymentId });
                    }
                }
            }
            catch(Exception ex)
            {
                string message = string.Format("Error occured! {0}", ex.Message);
                SetMessage(message, Message.Category.Error);
            }

            LoadviewBags();
            return View(viewModel);
        }

        //create Payment When Transcript Request is made
        public Payment CreatePayment(FeeType feeType, Person person)
        {
            var paymentLogic = new PaymentLogic();
            var payment = new Payment();
            var sessionLogic = new SessionLogic();
            var session = sessionLogic.GetAll().LastOrDefault();

            try
            {
                //Add PaymentMode, Session, PersonType [Both as Entities AND AS FK], PaymentType
                //personType: PersonTypeId, PersonTypeName, Active: true
                //Create tables for onlinePayment
                payment.FeeType = feeType;
                payment.Session = session;
                payment.PaymentType = new Payment_Type { PaymentTypeId = 1 };
                payment.DateCreated = DateTime.Now;
                payment.Person = person;
                payment.PersonType = new PersonType { PersonTypeId = (int)PersonTypeList.Graduate };

                return paymentLogic.Create(payment);
            }
            catch(Exception ex) { throw ex; }
            
        }
        
        [HttpPost]
        public ActionResult OpenTranscriptRequest(string MatricNumber)
        {//search for the persons details and then prepolute the fields
            if (MatricNumber != null)
            {
                viewModel.MatricNumber = MatricNumber;
                TempData["MatricNumber"] = MatricNumber;

                //check to see if user with matric Number already exists in db.Person
                var personLogic = new PersonLogic();
                var person = personLogic.GetModelsBy(p => p.MatricNumber == MatricNumber).LastOrDefault();

                if (person != null)
                {
                    ViewBag.Person = person;
                    viewModel.MobileNo = person.PhoneNumber;
                    viewModel.Surname = person.Surname;
                    viewModel.OtherName = person.OtherName;
                    viewModel.FirstName = person.FirstName;
                }
            }

            return RedirectToAction("Transcript", "Home");
        }

        public ActionResult ViewInvoice(long? payId)
        {
            var paymentLogic = new PaymentLogic();
            var model = new ViewInvoiceViewModel();

            try
            {
                if (payId < 1)
                {
                    //redirct the user back
                    return RedirectToAction("ViewInvoice", "Home");
                }

                var invoice = paymentLogic.GetModelsBy(p => p.PaymentId == payId).LastOrDefault();
                if (invoice == null)
                {
                    //redirct the user back
                    return RedirectToAction("ViewInvoice", "Home");
                }
                model.Amount = invoice.AmountPaid;
                //get the Person node
                var person = invoice.Person;
                model.Person = string.Format("{0} {1} {2}", person.Surname,person.FirstName, person.OtherName);
                model.DateCreated = invoice.DateCreated;
                model.RRR = invoice.RRR;
            }
            catch(Exception ex) { throw ex; }

            return View(model);
        }

        //Generate Reciept
        public ActionResult GenerateRecipt(long payId)
        {
            if (payId < 1)
            {
                return RedirectToAction("Transcript", "Home");
            }
            return View();
        }

        [HttpPost]
        public ActionResult GenerateReicipt(string RRR)
        {
            //Make Request to REMITA
            return View();
        }

        //Redirect after the Submission of a transcript Request
        public ActionResult ProcessRemitaPayment(int Id)
        {
            var viewModel = new ProcessRemitaPaymentViewModel();

            try
            {   
                PaymentLogic paymentLogic = new PaymentLogic();
                //get payment by sessionId and RequestId
                Payment payment = paymentLogic.GetModelsBy(p => p.PaymentId == Id).FirstOrDefault();

                var remitaPaymentSettingsLogic = new RemitaPaymentSettingsLogic();
                //get remitaSettings
                RemitaPaymentSettings remitaSettings = remitaPaymentSettingsLogic.GetModelsBy(re => re.Id == 1).FirstOrDefault();

                if (payment != null)
                {
                    var personDetails = payment.Person;
                    viewModel.PaymentId = payment.PaymentId;
                    viewModel.RemitaMerchantId = remitaSettings.MerchantId;
                    viewModel.RemitaServiceTypeId = remitaSettings.ServiceTypeId;
                    viewModel.RemitaOrderId = payment.InvoiceNumber;
                    //Get remitaAmount from Payment.FeeDetail
                    //viewModel.RemitaPaymentTotalAmount = (decimal);
                    viewModel.RemitaPayerName = string.Format("{0} {1} {2}", personDetails.FirstName, personDetails.Surname, personDetails.OtherName);
                    viewModel.RemitaPayerEmail = !string.IsNullOrEmpty(personDetails.Email) ? personDetails.Email : "test@lloydant.com";
                    viewModel.RemitaPayerPhone = personDetails.PhoneNumber;
                    //viewModel.RemitaResponseURL = "http://ndapplication.fpno.edu.ng/Applicant/PostUtmeResult/RemitaResponse";
                    viewModel.RemitaResponseURL = "http://localhost:47649/Applicant/PostUtmeResult/RemitaResponse";
                    viewModel.PaymentType = "RRRGEN";
                    //viewModel.RemitaAmount = Amt.ToString().split('.')[0];
                }

                string hash_string = viewModel.RemitaMerchantId + viewModel.RemitaServiceTypeId + viewModel.RemitaOrderId + viewModel.RemitaAmount + viewModel.RemitaResponseURL + remitaSettings.Api_Key;
                System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
                Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hash_string));
                sha512.Clear();
                string hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();

                //set the Hashed string of the dataPayload
                viewModel.RemitaHash = hashed;

                TempData["ProcessRemitaPayment"] = viewModel;
            }
            catch(Exception ex)
            {
                string message = string.Format("Error Occured! {0}", ex.Message);
                SetMessage(message, Message.Category.Error);
            }

            return View(viewModel);
        }

        public ActionResult RemitaResponse(string OrderID)
        {
            RemitaResponse remitaResponse = new RemitaResponse();
            ProcessRemitaPaymentViewModel viewModel = (ProcessRemitaPaymentViewModel)TempData["ProcessRemitaPayment"];

            if (viewModel != null)
            {
                viewModel = new ProcessRemitaPaymentViewModel();
            }

            try
            {
                string merchant_id = "532776942";
                string apiKey = "587460";
                string hashed;
                string checkstatusurl = ConfigurationManager.AppSettings["RemitaVerifyUrl"].ToString();
                string url;
                string orderID = Request.QueryString["orderID"];

                if (orderID != null)
                {
                    PaymentLogic paymentLogic = new PaymentLogic();
                    PaymentRRRLogic paymentRRRLogic = new PaymentRRRLogic();
                    Payment_RRR remitaPayment = new Payment_RRR();

                    orderID = Request.QueryString["orderID"].ToString();
                    //get the list of payments
                    Payment payment = paymentLogic.GetModelsBy(p => p.PaymentId == viewModel.PaymentId).FirstOrDefault();
                    if (payment != null)
                    {
                        //get the PaymentRRR
                        remitaPayment = paymentRRRLogic.GetModelBy(r => r.PaymentId == viewModel.PaymentId);
                    }
                    else
                    {
                        remitaPayment = null;
                    }

                    string hash_string = orderID + apiKey + merchant_id;
                    System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
                    Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hash_string));
                    sha512.Clear();
                    hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();
                    url = checkstatusurl + "/" + merchant_id + "/" + orderID + "/" + hashed + "/" + "orderstatus.reg";


                    viewModel.RemitaHash = hashed;

                    if (remitaPayment == null)
                    {
                        string jsondata = new WebClient().DownloadString(url);
                        remitaResponse = JsonConvert.DeserializeObject<RemitaResponse>(jsondata);
                    }

                    if (remitaPayment == null)
                    {
                        string jsondata = new WebClient().DownloadString(url);
                        remitaResponse = JsonConvert.DeserializeObject<RemitaResponse>(jsondata);
                    }

                    if (remitaResponse != null && remitaResponse.Status != null)
                    {
                        if (payment != null)
                        {
                            if (remitaPayment != null)
                            {
                                remitaPayment.Status = remitaResponse.Status + ":" + remitaResponse.StatusCode;
                                paymentRRRLogic.Modify(remitaPayment);
                            }
                            else
                            {
                                remitaPayment = new Payment_RRR();
                                remitaPayment.Payment = payment;
                                remitaPayment.RRR = remitaResponse.rrr;
                                remitaPayment.OrderId = payment.InvoiceNumber;
                                remitaPayment.Status = remitaResponse.Status;
                                remitaPayment.TransactionAmount = remitaResponse.amount;
                                remitaPayment.DateCreated = DateTime.Now;
                                remitaPayment.MerchantCode = merchant_id;
                                remitaPayment.Description = "SCREENING CHECKING PIN";
                                if (paymentRRRLogic.GetModelBy(p => p.PaymentId == payment.PaymentId) == null)
                                {
                                    paymentRRRLogic.Create(remitaPayment);
                                }
                            }

                            var personDetails = payment.Person;
                            viewModel.RemitaPayerEmail = personDetails.Email;
                            viewModel.RemitaPayerPhone = personDetails.PhoneNumber;
                            viewModel.RemitaPayerName = string.Format("{0} {1} {2}", personDetails.Surname, personDetails.FirstName, personDetails.OtherName);
                        }

                        TempData["ProcessRemitaPayment"] = viewModel;
                    }
                }
                else
                {
                    SetMessage("Order ID was not generated from this system", Message.Category.Error);
                }

                return View(viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        //Process the Response coming from Remita
        //public ActionResult RemitaResponse(string orderID)
        //{
        //    RemitaResponse remitaResponse = new RemitaResponse();
        //    PostUtmeResultViewModel viewModel = (PostUtmeResultViewModel)TempData["ScreeningCheckingPayment"];
        //    if (viewModel == null)
        //    {
        //        viewModel = new PostUtmeResultViewModel();
        //    }

        //    try
        //    {
        //        string merchant_id = "532776942";
        //        string apiKey = "587460";
        //        string hashed;
        //        string checkstatusurl = ConfigurationManager.AppSettings["RemitaVerifyUrl"].ToString();
        //        string url;

        //        if (Request.QueryString["orderID"] != null)
        //        {
        //            PaymentLogic paymentLogic = new PaymentLogic();
        //            RemitaPaymentLogic remitaLogic = new RemitaPaymentLogic();
        //            AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
        //            ApplicationProgrammeFeeLogic programmeFeeLogic = new ApplicationProgrammeFeeLogic();
        //            AppliedCourse appliedCourse = new AppliedCourse();
        //            ApplicationProgrammeFee programmeFee = new ApplicationProgrammeFee();
        //            RemitaPayment remitaPyament = new RemitaPayment();

        //            orderID = Request.QueryString["orderID"].ToString();

        //            Payment payment = paymentLogic.GetBy(orderID, viewModel.Programme);

        //            if (payment != null)
        //            {
        //                remitaPyament = remitaLogic.GetModelBy(p => p.Payment_Id == payment.Id);
        //                appliedCourse = appliedCourseLogic.GetModelsBy(a => a.Person_Id == payment.Person.Id).LastOrDefault();
        //                if (appliedCourse != null)
        //                {
        //                    //programmeFee = programmeFeeLogic.GetModelBy(p => p.Fee_Type_Id == payment.FeeType.Id && p.Session_Id == payment.Session.Id && p.Programme_Id == appliedCourse.Programme.Id);

        //                }
        //            }
        //            else
        //            {
        //                remitaPyament = null;
        //            }

        //            string hash_string = orderID + apiKey + merchant_id;
        //            System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
        //            Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hash_string));
        //            sha512.Clear();
        //            hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();
        //            url = checkstatusurl + "/" + merchant_id + "/" + orderID + "/" + hashed + "/" + "orderstatus.reg";


        //            //viewModel.Hash = hashed;

        //            if (remitaPyament == null)
        //            {
        //                string jsondata = new WebClient().DownloadString(url);
        //                remitaResponse = JsonConvert.DeserializeObject<RemitaResponse>(jsondata);
        //            }

        //            if (remitaResponse != null && remitaResponse.Status != null)
        //            {
        //                if (payment != null)
        //                {
        //                    if (remitaPyament != null)
        //                    {
        //                        remitaPyament.Status = remitaResponse.Status + ":" + remitaResponse.StatusCode;
        //                        remitaLogic.Modify(remitaPyament);
        //                    }
        //                    else
        //                    {
        //                        remitaPyament = new RemitaPayment();
        //                        remitaPyament.payment = payment;
        //                        remitaPyament.RRR = remitaResponse.rrr;
        //                        remitaPyament.OrderId = remitaResponse.orderId;
        //                        remitaPyament.Status = remitaResponse.Status;
        //                        remitaPyament.TransactionAmount = remitaResponse.amount;
        //                        remitaPyament.TransactionDate = DateTime.Now;
        //                        remitaPyament.MerchantCode = merchant_id;
        //                        remitaPyament.Description = "SCREENING CHECKING PIN";
        //                        if (remitaLogic.GetBy(payment.Id) == null)
        //                        {
        //                            remitaLogic.Create(remitaPyament);
        //                        }
        //                    }

        //                    viewModel.RemitaPayment = remitaPyament;
        //                    viewModel.Payment = payment;
        //                    viewModel.Person = payment.Person;
        //                }//stop
        //                else
        //                {
        //                    RemitaPayment remitaPayment = remitaLogic.GetBy(orderID);
        //                    string remitaVerifyUrl = ConfigurationManager.AppSettings["RemitaVerifyUrl"].ToString();
        //                    RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(apiKey);
        //                    remitaResponse = remitaProcessor.TransactionStatus(remitaVerifyUrl, remitaPayment);
        //                    if (remitaResponse != null && remitaResponse.Status != null)
        //                    {
        //                        remitaPayment.Status = remitaResponse.Status + ":" + remitaResponse.StatusCode;
        //                        remitaLogic.Modify(remitaPayment);

        //                        payment = paymentLogic.GetBy(remitaPayment.payment.Id);
        //                        viewModel.RemitaPayment = remitaPayment;
        //                        if (payment != null)
        //                        {
        //                            viewModel.Payment = payment;
        //                            viewModel.Person = payment.Person;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        SetMessage("Payment does not exist!", Message.Category.Error);
        //                    }

        //                    viewModel.RemitaPayment = remitaPayment;
        //                }


        //                string hash = "532776942" + remitaResponse.rrr + "587460";
        //                RemitaPayementProcessor myRemitaProcessor = new RemitaPayementProcessor(hash);
        //                viewModel.Hash = myRemitaProcessor.HashPaymentDetailToSHA512(hash);
        //            }
        //            else
        //            {
        //                SetMessage("Order ID was not generated from this system", Message.Category.Error);
        //            }

        //            viewModel.Department = appliedCourse != null ? appliedCourse.Department : new Department();
        //            viewModel.ApplicationProgrammeFee = programmeFee != null ? programmeFee : new ApplicationProgrammeFee();
        //            viewModel.AppliedCourse = appliedCourse;
        //        }
        //        else
        //        {
        //            SetMessage("No data was received!", Message.Category.Error);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SetMessage("Error! " + ex.Message, Message.Category.Error);
        //    }

        //    TempData["ScreeningCheckingPayment"] = viewModel;
        //    return View(viewModel);
        //}

        //Method for Process Payments made to Remita
        //public ActionResult ProcessRemitaPayment()
        //{
        //    PostUtmeResultViewModel viewModel = (PostUtmeResultViewModel)TempData["ScreeningCheckingPayment"];
        //    try
        //    {
        //        if (viewModel != null)
        //        {
        //            PaymentLogic paymentLogic = new PaymentLogic();

        //            Payment payment = paymentLogic.GetBy(viewModel.Payment.Id, viewModel.Programme);
        //            if (payment != null)
        //            {
        //                //Get Payment Specific Setting
        //                decimal Amt = 0;
        //                Amt = payment.FeeDetails.Sum(p => p.Fee.Amount);
        //                RemitaPaymentSettings settings = new RemitaPaymentSettings();
        //                RemitaPaymentSettingsLogic settingsLogic = new RemitaPaymentSettingsLogic();
        //                settings = settingsLogic.GetBy(4);
        //                var personDetails = payment.Person;

        //                Remita remita = new Remita()
        //                {
        //                    merchantId = settings.MerchantId,
        //                    serviceTypeId = settings.ServiceTypeId,
        //                    orderId = payment.InvoiceNumber.ToString(),
        //                    totalAmount = (decimal)Amt,
        //                    payerName = string.Format("{0} {1} {2}", personDetails.FirstName, personDetails.Surname, personDetails.OtherName),
        //                    payerEmail = !string.IsNullOrEmpty(payment.Person.Email) ? payment.Person.Email : "test@lloydant.com",
        //                    payerPhone = payment.Person.PhoneNumber,
        //                    responseurl = "http://ndapplication.fpno.edu.ng/Applicant/PostUtmeResult/RemitaResponse",
        //                    //responseurl = "http://localhost:2600/Applicant/PostUtmeResult/RemitaResponse",
        //                    paymenttype = "RRRGEN",
        //                    amt = Amt.ToString()
        //                };

        //                remita.amt = remita.amt.Split('.')[0];

        //                string hash_string = remita.merchantId + remita.serviceTypeId + remita.orderId + remita.amt + remita.responseurl + settings.Api_key;
        //                System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
        //                Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hash_string));
        //                sha512.Clear();
        //                string hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();

        //                remita.hash = hashed;

        //                viewModel.Remita = remita;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
        //    }

        //    TempData["ScreeningCheckingPayment"] = viewModel;

        //    return View(viewModel);
        //}

        private Payment CreateTranscriptRequest(TranscriptRequestViewModel viewModel)
        {
            var genderLogic = new GenderLogic();
            var personLogic = new PersonLogic();
            var deliveryServiceLogic = new DeliveryServiceLogic();
            var feeTypeLogic = new FeeTypeLogic();
            var feeDetailLogic = new FeeDetailLogic();
            var programmeLogic = new ProgrammeLogic();
            var departmentLogic = new DepartmentLogic();
            var levelLogic = new LevelLogic();
            var stateLogic = new StateLogic();
            var requestLogic = new RequestLogic();
            var countryLogic = new CountryLogic();
            Payment payment = null;

            using (TransactionScope transactionScope = new TransactionScope())
            {
                //check if the person already exists
                var personReturned = personLogic.GetModelsBy(per => per.MatricNumber == viewModel.MatricNumber).LastOrDefault();

                if (personReturned == null)
                {
                    //save the person data
                    Person person = new Person()
                    {
                        Surname = viewModel.Surname,
                        OtherName = viewModel.OtherName,
                        FirstName = viewModel.FirstName,
                        PhoneNumber = viewModel.MobileNo,
                        Gender = genderLogic.GetModelsBy(g => g.GenderId == viewModel.Gender).LastOrDefault(),
                        Email = viewModel.Email,
                        MatricNumber = viewModel.MatricNumber
                    };

                    personReturned = personLogic.Create(person);
                }

                Request request = new Request();
                request.Person = personReturned;
                request.Level = levelLogic.GetModelsBy(l => l.LevelId == viewModel.Level).LastOrDefault();
                request.DestinationAddress = viewModel.DestinationLocation;
                request.Country = countryLogic.GetModelsBy(c => c.CountryId == viewModel.DestinationCountry).LastOrDefault();
                if (viewModel.Courier != null)
                {
                    request.DeliveryService = deliveryServiceLogic.GetModelsBy(ds => ds.DeliveryServiceId == viewModel.Courier).LastOrDefault();
                }
                if (viewModel.State != null && viewModel.DestinationCountry != 2)//ID: 2 FOR OTHERS
                {
                    request.State = stateLogic.GetModelsBy(st => st.StateCode == viewModel.State).LastOrDefault();
                }
                request.Programme = programmeLogic.GetModelsBy(p => p.ProgrammeId == viewModel.Programme).LastOrDefault();
                request.FeeType = feeTypeLogic.GetModelsBy(ft => ft.FeeTypeId == viewModel.RequestType).LastOrDefault();
                request.Department = departmentLogic.GetModelsBy(d => d.DepartmentId == viewModel.Department).LastOrDefault();
                request.DateCreated = DateTime.Now;

                var req = requestLogic.Create(request);

                //make request to payment tbl
                //create an invoice number for the user
                //make an entry into the payment table
                //////Make Column: AmountPaid: Decimal
                var invoiceNumberLogic = new InvoiceNumberGenerationLogic();
                var invoiceNumber = invoiceNumberLogic.CreateInvoiceNumber();

                //retrieve the data models to be used in payment
                var feeType = feeTypeLogic.GetModelsBy(ft => ft.FeeTypeId == viewModel.RequestType).LastOrDefault();
                var FeeDetail = feeDetailLogic.GetModelsBy(fd => fd.FeeTypeId == feeType.FeeTypeId).LastOrDefault();

                //Create Payment
                payment = CreatePayment(feeType, personReturned);
                //var paymentLogic = new PaymentLogic();
                //var payment = new Payment()
                //{
                //    FeeType = feeType,
                //    Person = personReturned,
                //    InvoiceNumber = invoiceNumber,
                //    AmountPaid = FeeDetail.Fee.Amount,
                //    Request = req,
                //    DateCreated = DateTime.Now
                //};

                //paymentLogic.Create(payment);

                transactionScope.Complete();
            }
            return payment;
        }

        private void UpdateTranscriptRequest(Person person, Request request, TranscriptRequestViewModel viewModel)
        {
            var personLogic = new PersonLogic();
            var requestLogic = new RequestLogic();

            person.FirstName = viewModel.FirstName;
            person.Surname = viewModel.Surname;
            person.OtherName = viewModel.OtherName;
            person.PhoneNumber = viewModel.MobileNo;
            person.Gender = new GenderLogic().GetModelsBy(g => g.GenderId == viewModel.Gender).LastOrDefault();
            person.Email = viewModel.Email;

            request.Level = new LevelLogic().GetModelsBy(l => l.LevelId == viewModel.Level).LastOrDefault();
            request.Department = new DepartmentLogic().GetModelsBy(dept => dept.DepartmentId == viewModel.Department).LastOrDefault();
            request.Programme = new ProgrammeLogic().GetModelsBy(prog => prog.ProgrammeId == viewModel.Programme).LastOrDefault();
            request.DestinationAddress = viewModel.DestinationLocation;
            request.Country = new CountryLogic().GetModelsBy(c => c.CountryId == viewModel.DestinationCountry).LastOrDefault();
            request.State = new StateLogic().GetModelsBy(st => st.StateCode == viewModel.State).LastOrDefault();
            request.FeeType = new FeeTypeLogic().GetModelsBy(st => st.FeeTypeId == viewModel.RequestType).LastOrDefault();
            request.DeliveryService = new DeliveryServiceLogic().GetModelsBy(ds => ds.DeliveryServiceId == viewModel.Courier).LastOrDefault();

            using (TransactionScope transactionScope = new TransactionScope())
            {
                personLogic.Modify(person);
                requestLogic.Modify(request); ;

                transactionScope.Complete();
            }
        }
        
        private void LoadviewBags()
        {
            try
            {
                var programmes = viewModel.GetProgrammes();
                var levels = viewModel.GetLevels();
                var deliveryServices = viewModel.GetDeliveryServices();
                var feeTypes = viewModel.GetFeeTypes();
                var states = viewModel.GetStates();
                var countries = viewModel.GetCountries();
                var departments = viewModel.GetDepartments();
                var genders = viewModel.GetGenders();

                //set up viewBags
                if (programmes != null)
                {
                    ViewBag.Programme = programmes;
                }
                if (levels != null)
                {
                    ViewBag.Levels = levels;
                }
                if (deliveryServices != null)
                {
                    ViewBag.DeliveryServices = deliveryServices;
                }

                if (feeTypes != null)
                {
                    ViewBag.FeeType = feeTypes;
                }
                if (states != null)
                {
                    ViewBag.States = states;
                }
                if (countries != null)
                {
                    ViewBag.Countries = countries;
                }
                if (departments != null)
                {
                    ViewBag.Departments = departments;
                }
                if (genders != null)
                {
                    ViewBag.Genders = genders;
                }
            }
            catch(Exception ex) { throw ex; }
        }

    }
}