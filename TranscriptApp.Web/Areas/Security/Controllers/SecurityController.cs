﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TranscriptApp.Business;
using TranscriptApp.Web.Areas.Security.Models;

namespace TranscriptApp.Web.Areas.Security.Controllers
{
    public class SecurityController : Controller
    {
        private LoginViewModel viewModel;

        public SecurityController()
        {
            viewModel = new LoginViewModel();
        }
        
        // GET: Security/Security
        [AllowAnonymous]
        public ActionResult Index(string ReturnUrl = "")
        {
            if (!string.IsNullOrEmpty(ReturnUrl))
            {
                TempData["ReturnUrl"] = ReturnUrl;
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(LoginViewModel viewModel)
        {
            try
            {
                var userLogic = new UserLogic();
                var user = userLogic.GetModelsBy(u => u.Username == viewModel.Username && u.Password == viewModel.Password).LastOrDefault();

                if (user != null)
                {
                    FormsAuthentication.SetAuthCookie(viewModel.Username, false);
                    string name = User.Identity.Name;
                    string returnUrl = (string)TempData["ReturnUrl"];

                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return RedirectToLocal(returnUrl);
                    }

                    return RedirectToAction("Index", "AdminHome", new { Area = "Admin" });
                }

                ViewBag.Message = "Wrong Credentials";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(viewModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Security", new { Area = "Security" });
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Security", new { Area = "Security" });
            }
        }
    }
}