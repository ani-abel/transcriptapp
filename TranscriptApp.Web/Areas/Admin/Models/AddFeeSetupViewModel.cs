﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TranscriptApp.Business;
using TranscriptApp.Model.Model;

namespace TranscriptApp.Web.Areas.Admin.Models
{
    public class AddFeeSetupViewModel
    {
        [Required]
        public int Session { get; set; }

        [Required]
        [DisplayName("Fee Type")]
        public int FeeType { get; set; }

        [Required]
        public int Amount { get; set; }

        public List<Value> GetSessions()
        {
            try
            {
                var sessionLogic = new SessionLogic();
                List<Session> Sessions = sessionLogic.GetModelsBy(sess => sess.Active);
                List<Value> Values = new List<Value>();

                if (Sessions != null && Sessions.Count > 0)
                {
                    for (int i = 0; i < Sessions.Count; i++)
                    {
                        Values.Add(new Value() { Id = Sessions[i].SessionId, ValueText = Sessions[i].SessionName });
                    }
                }
                return Values;
            }

            catch(Exception ex) { throw ex; }
        }

        public List<Value> GetFeeTypes()
        {
            try
            {
                var feeTypeLogic = new FeeTypeLogic();
                List<FeeType> FeeTypes = feeTypeLogic.GetModelsBy(ft => ft.Active);
                List<Value> Values = new List<Value>();

                if (FeeTypes != null && FeeTypes.Count > 0)
                {
                    for (int i = 0; i < FeeTypes.Count; i++)
                    {
                        Values.Add(new Value() { Id = FeeTypes[i].FeeTypeId, ValueText = FeeTypes[i].FeeTypeName });
                    }
                }
                return Values;
            }

            catch (Exception ex) { throw ex; }
        }

        public List<Value> GetFees()
        {
            try
            {
                var feeLogic = new FeeLogic();
                List<Fee> Fees = feeLogic.GetModelsBy(f => f.Active);
                List<Value> Values = new List<Value>();

                if (Fees != null && Fees.Count > 0)
                {
                    for (int i = 0; i < Fees.Count; i++)
                    {
                        Values.Add(new Value() { Id = Fees[i].FeeId, ValueText = Fees[i].Amount.ToString() });
                    }
                }
                return Values;
            }

            catch (Exception ex) { throw ex; }
        }
    }
}