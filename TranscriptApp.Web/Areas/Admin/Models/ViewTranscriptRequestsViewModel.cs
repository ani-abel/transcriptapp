﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TranscriptApp.Web.Areas.Admin.Models
{
    public class ViewTranscriptRequestsViewModel
    {
        public string FullName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Programme { get; set; }

        public string Gender { get; set; }

        public string Level { get; set; }

        public string Department { get; set; }

        public string FeeType { get; set; }

        public string DestinationAddress { get; set; }
    }
}