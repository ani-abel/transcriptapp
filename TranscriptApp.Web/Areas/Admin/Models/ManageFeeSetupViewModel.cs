﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TranscriptApp.Web.Areas.Admin.Models
{
    public class ManageFeeSetupViewModel
    {
        public int FeeSetupId { get; set; }

        public string FeeType { get; set; }

        public decimal Amount { get; set; }

        public string Session { get; set; }

        public bool IsActivated { get; set; }
    }
}