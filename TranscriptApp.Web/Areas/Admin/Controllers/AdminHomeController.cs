﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TranscriptApp.Business;
using TranscriptApp.Model.Model;
using TranscriptApp.Web.Areas.Admin.Models;

namespace TranscriptApp.Web.Areas.Admin.Controllers
{
    [Authorize]
    public class AdminHomeController : Controller
    {
        public AddFeeSetupViewModel AddFeeSetupViewModel;

        public AdminHomeController()
        {
            AddFeeSetupViewModel = new AddFeeSetupViewModel();
        }

        // GET: Admin/Home
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult ManageFeeSetup()
        {
            var viewModel = GetFeeDetsils();

            return View(viewModel);
        }

        public JsonResult ModifyFeeDetail(int FeeDetailId, string FeeDetailStatus)
        {
            try
            {
                var FeeDetailLogic = new FeeDetailLogic();
                var FeeDetailModel = FeeDetailLogic.GetModelsBy(fee => fee.FeeDetailId == FeeDetailId).LastOrDefault();
                FeeDetailModel.Active = FeeDetailStatus.ToLower() == "true" ? false : true;

                if (FeeDetailModel != null)
                {
                    FeeDetailLogic.Modify(FeeDetailModel);
                }
                var JsonPayload = new JsonPayload() { Message = "Fee Detail Status was changed", WasSuccessFul = true };

                return Json(JsonPayload, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex) { throw ex; }
        }

        //[Authorize]
        public ActionResult AddFeeSetup()
        {
            LoadFeeSetupViewBags();
            var username=User.Identity.Name;

            return View();
        }
        
        [HttpPost]
        public ActionResult AddFeeSetup(AddFeeSetupViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var FeeDetailLogic = new FeeDetailLogic();
                    var FeeDetail = new FeeDetail()
                    {
                        Fee = new FeeLogic().GetModelsBy(fee => fee.FeeId == viewModel.Amount).LastOrDefault(),
                        FeeType = new FeeTypeLogic().GetModelsBy(ft => ft.FeeTypeId == viewModel.FeeType).LastOrDefault(),
                        Session = new SessionLogic().GetModelsBy(sess => sess.SessionId == viewModel.Session).LastOrDefault(),
                        Active = true
                    };

                    var FeeDetailReturned = FeeDetailLogic.Create(FeeDetail);

                    if (FeeDetailReturned != null)
                    {
                        LoadFeeSetupViewBags();

                        return View();
                    }
                }
                return View(viewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ViewTranscriptRequests()
        {
            var requestLogic = new RequestLogic();
            var requestList = requestLogic.GetAll().OrderBy(req => req.DateCreated).ToList();
            List<ViewTranscriptRequestsViewModel> viewModel = new List<ViewTranscriptRequestsViewModel>();

            if (requestList != null)
            {
                for (int i = 0; i < requestList.Count(); i++)
                {
                    var model = new ViewTranscriptRequestsViewModel()
                    {
                            FullName = string.Format("{0} {1} {2}", requestList[i].Person.Surname, requestList[i].Person.FirstName, requestList[i].Person.OtherName),
                            Email = requestList[i].Person.Email,
                            Gender = requestList[i].Person.Gender.GenderName,
                            PhoneNumber = requestList[i].Person.PhoneNumber,
                            Programme = requestList[i].Programme.ProgrammeName,
                            Department = requestList[i].Department.DepartmentName,
                            Level = requestList[i].Level.LevelName,
                            FeeType = requestList[i].FeeType.FeeTypeName,
                            DestinationAddress = requestList[i].DestinationAddress
                    };

                    viewModel.Add(model);
                }
                
            }
            
            return View(viewModel);
        }

        //HELPER METHODS
        private void LoadFeeSetupViewBags()
        {
            var Sessions = AddFeeSetupViewModel.GetSessions();
            var FeeTypes = AddFeeSetupViewModel.GetFeeTypes();
            var Amounts = AddFeeSetupViewModel.GetFees();

            if (Sessions != null)
            {
                ViewBag.Sessions = Sessions;
            }
            if (FeeTypes != null)
            {
                ViewBag.FeeTypes = FeeTypes;
            }
            if (Amounts != null)
            {
                ViewBag.Amounts = Amounts;
            }
        }

        private List<ManageFeeSetupViewModel> GetFeeDetsils()
        {
            var feeDetailLogic = new FeeDetailLogic();
            var feeDetails = feeDetailLogic.GetAll().OrderBy(fd => fd.Active).ToList();
            List<ManageFeeSetupViewModel> viewModel = new List<ManageFeeSetupViewModel>();

            if (feeDetails != null && feeDetails.Count > 0)
            {
                for (int i = 0; i < feeDetails.Count; i++)
                {
                    var model = new ManageFeeSetupViewModel()
                    {
                        FeeSetupId = feeDetails[i].FeeDetailId,
                        FeeType = feeDetails[i].FeeType.FeeTypeName,
                        Amount = feeDetails[i].Fee.Amount,
                        Session = feeDetails[i].Session.SessionName,
                        IsActivated = feeDetails[i].Active
                    };

                    viewModel.Add(model);
                }
            }

            return viewModel;
        }
    }
}