﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;
using TranscriptApp.Model.Translator;

namespace TranscriptApp.Business
{
    public class FeeDetailLogic: BusinessBaseLogic<FeeDetail, FEE_DETAIL>
    {
        public FeeDetailLogic()
        {
            translator = new FeeDetailTranslator();
        }


        public bool Modify(FeeDetail feeDetail)
        {
            try
            {
                FEE_DETAIL entity = GetEntityBy(fd => fd.FeeDetailId == feeDetail.FeeDetailId);

                //toggle the Active field
                entity.Active = feeDetail.Active;

                int modifiedRecordCount = Save();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
