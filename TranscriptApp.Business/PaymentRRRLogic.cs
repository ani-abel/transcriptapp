﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;
using TranscriptApp.Model.Translator;

namespace TranscriptApp.Business
{
    public class PaymentRRRLogic: BusinessBaseLogic<Payment_RRR, PAYMENT_RRR>
    {
        public PaymentRRRLogic()
        {
            translator = new PaymentRRRTranslator();
        }

        public bool Modify(Payment_RRR remitaPayment)
        {
            try
            {
                PAYMENT_RRR entity = GetEntityBy(p => p.PaymentId == remitaPayment.Payment.PaymentId);
                if (entity != null)
                {
                    throw new Exception("No matching entity was found");
                }

                entity.RRR = remitaPayment.RRR;
                entity.Status = remitaPayment.Status;

                if (remitaPayment.BankCode != null)
                {
                    entity.BankCode = remitaPayment.BankCode;
                }
                if (remitaPayment.MerchantCode != null)
                {
                    entity.MerchantCode = remitaPayment.MerchantCode;
                }

                if (remitaPayment.TransactionAmount > 0)
                {
                    entity.Amount = remitaPayment.TransactionAmount;
                }

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
