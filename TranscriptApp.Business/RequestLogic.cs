﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;
using TranscriptApp.Model.Translator;

namespace TranscriptApp.Business
{
    public class RequestLogic : BusinessBaseLogic<Request, REQUEST>
    {
        public RequestLogic()
        {
            translator = new RequestTranslator();
        }

        public bool Modify(Request request)
        {
            try
            {
                REQUEST entity = GetEntityBy(req => req.PersonId == request.Person.PersonId);

                if (request.Level != null)
                {
                    entity.LevelId = request.Level.LevelId;
                }
                if (request.Programme != null)
                {
                    entity.ProgrammeId = request.Programme.ProgrammeId;
                }
                if (request.Department != null)
                {
                    entity.DepartmentId = request.Department.DepartmentId;
                }
                if (request.DestinationAddress != null)
                {
                    entity.DestinationAddress = request.DestinationAddress;
                }
                if (request.Country != null)
                {
                    entity.CountryId = request.Country.CountryId;
                }
                if (request.State != null)
                {
                    entity.StateId = request.State.StateCode;
                }
                else
                {
                    entity.StateId = null;
                }

                if (request.DeliveryService != null)
                {
                    entity.DeliveryServiceId = request.DeliveryService.DeliveryServiceId;
                }
                else
                {
                    entity.DeliveryServiceId = null;
                }

                if (request.FeeType != null)
                {
                    entity.FeeTypeId = request.FeeType.FeeTypeId;
                }

                int modifiedRecordCount = Save();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }
    }
        
}
