﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;
using TranscriptApp.Model.Translator;

namespace TranscriptApp.Business
{
    public class PersonLogic: BusinessBaseLogic<Person, PERSON>
    {
        public PersonLogic()
        {
            translator = new PersonTranslator();
        }

        public bool Modify(Person person)
        {
            try
            {
                PERSON entity = GetEntityBy(per => per.PersonId == person.PersonId);

                if (person.Surname != null)
                {
                    entity.Surname = person.Surname;
                }
                if (person.FirstName !=null)
                {
                    entity.FirstName = person.FirstName;
                }
                if (person.OtherName != null)
                {
                    entity.OtherName = person.OtherName;
                }
                if (person.PhoneNumber != null)
                {
                    entity.PhoneNumber = person.PhoneNumber;
                }
                if (person.Gender != null)
                {
                    entity.GenderId = person.Gender.GenderId;
                }
                if (person.Email != null)
                {
                    entity.Email = person.Email;
                }

                int modifiedRecordCount = Save();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }
        
    }
}
