﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Translator;
using TranscriptApp.Model.Entity;

namespace TranscriptApp.Business
{
    public class RemitaPaymentSettingsLogic: BusinessBaseLogic<RemitaPaymentSettings, REMITA_PAYMENT_SETTINGS>
    {
        public RemitaPaymentSettingsLogic()
        {
            translator = new RemitaPaymentSettingsTranslator();
        }
    }
}
