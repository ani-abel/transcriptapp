﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;
using TranscriptApp.Model.Translator;

namespace TranscriptApp.Business
{
    public class DeliveryServiceLogic: BusinessBaseLogic<DeliveryService, DELIVERY_SERVICE>
    {
        public DeliveryServiceLogic()
        {
            translator = new DeliveryServiceTranslator();
        }
    }
}
