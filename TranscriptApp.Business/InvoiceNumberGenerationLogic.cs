﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranscriptApp.Model.Model;
using TranscriptApp.Model.Entity;
using TranscriptApp.Model.Translator;

namespace TranscriptApp.Business
{
    public class InvoiceNumberGenerationLogic: BusinessBaseLogic<InvoiceNumberGeneration, INVOICE_NUMBER_GENERATION>
    {
        public InvoiceNumberGenerationLogic()
        {
            translator = new InvoiceNumberGenerationTranslator();
        }

        //update the Invoice Number generation table
        public bool Modify(InvoiceNumberGeneration model)
        {
            try
            {
                INVOICE_NUMBER_GENERATION entity = GetEntityBy(ng => ng.Prefix == model.Prefix && ng.Active);

                if (entity != null)
                {
                    entity.StartFrom = model.StartFrom + 1;
                }

                int modifiedRecordCount = Save();
                return true;
            }
            catch(Exception ex) { throw ex; }
        }

        public string CreateInvoiceNumber()
        {
            try
            {
                var model = GetModelsBy(ng => ng.Active).LastOrDefault();
                string invoiceNumber = "";
                if(model != null) {
                    string prefix = model.Prefix;
                    long startFrom = model.StartFrom;
                    int year = DateTime.Now.Year;
                    //convert to string
                    string yr = year.ToString().Substring(2, 2);
                    invoiceNumber = string.Format("{0}{1}{2}", prefix, yr, AppendZeros(startFrom));
                }

                //modify the startFrom Value
                Modify(model);
                return invoiceNumber;

            }
            catch(Exception ex) {
                throw ex; }
        }

        public string AppendZeros(long startFrom)
        {
            try
            {
                //convert to int so you can find the number of zeroes needed
                int noOfZeros = (int)(10 - startFrom.ToString().Length);
                string finalString = "";
                int counter = 0;

                while (counter < noOfZeros)
                {
                    finalString += "0";
                    counter++;
                }
                finalString += startFrom;

                return finalString;
            }
            catch(Exception ex) { throw ex; }
        }
    }
}
